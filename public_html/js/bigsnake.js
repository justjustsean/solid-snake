/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */




var snake;
var snakeLength;
var snakeSize;
var snakeDirection;

var snake1;
var snakeLength1;
var snakeSize1;
var snakeDirection1;

var snake2;
var snakeLength2;
var snakeSize2;
var snakeDirection2;

var snake3;
var snakeLength3;
var snakeSize3;
var snakeDirection3;

var snake4;
var snakeLength4;
var snakeSize4;
var snakeDirection4;

var snake5;
var snakeLength5;
var snakeSize5;
var snakeDirection5;

var food;
var apple;
var block;

var context;
var screenWidth;
var screenHeight;


var gameState;
var gameOverMenu;
var restartButton;
var playHUD;
var scoreboard;
/*------------------------------------------------------------------------
 * Executing Game Code
 * -----------------------------------------------------------------------
 */

gameInitialize();
snakeInitialize();
snakeInitialize1();
snakeInitialize2();
snakeInitialize3();
snakeInitialize5();
foodInitialize();
appleInitialize();
blockInitialize();
setInterval(gameLoop, 1000/30);

/*
 * ----------------------------------------------------------------------
 *   Game Functions
 * -----------------------------------------------------------------------
 */

function gameInitialize() {
  var canvas = document.getElementById("game-screen");
  context = canvas.getContext("2d");
  
  screenWidth =  window.innerWidth;
  screenHeight = window.innerHeight;
  
  canvas.width = screenWidth;
  canvas.height = screenHeight;
  
  document.addEventListener("keydown", keyboardHandler);
  document.addEventListener("keydown", keyboardHandler1);
  document.addEventListener("keydown", keyboardHandler2);
  document.addEventListener("keydown", keyboardHandler3);
  document.addEventListener("keydown", keyboardHandler5);
  
  gameOverMenu = document.getElementById("gameOver");
  centerMenuPosition(gameOverMenu);
  
  restartButton = document.getElementById("restartButton");
  restartButton.addEventListener("click", gameRestart);
  
  playHUD = document.getElementById("playHUD");
  scoreboard =  document.getElementById("scoreboard");
  
  
  setState("PLAY"); 
  
}

function gameLoop() {
    gameDraw();
    drawScoreboard();
    if(gameState == "PLAY") {
    snakeUpdate();
    snakeUpdate1();
    snakeUpdate2();
    snakeUpdate3();
    snakeUpdate5();
    snakeDraw();
    snakeDraw1();
    snakeDraw2();
    snakeDraw3();
    snakeDraw5();
    foodDraw();
    appleDraw();
    blockDraw();
    snakeGrow();
    snakeEnlarge();
   
  
}
}

function gameDraw() {
   context.fillStyle = "green"; 
   context.fillRect(0, 0, screenWidth, screenHeight);
}

function gameRestart() {
   snakeInitialize();
   snakeInitialize1();
   snakeInitialize2();
   snakeInitialize3();
   snakeInitialize5();
   foodInitialize();
   appleInitialize();
   blockInitialize();
   hideMenu(gameOverMenu);
   setState("PLAY");
   
}

/*
 * ----------------------------------------------------------------------
 *   Snake Functions
 * -----------------------------------------------------------------------  
 */

function snakeInitialize() {
    snake = [];
    snakeLength = 5;
    snakeSize = 40;
    snakeDirection = "down";
    
    for(var index = snakeLength + 1; index >= 0; index--){
        snake.push( {
            x: index,
            y: 0
        } );
    }
}


function snakeDraw() {
    for(var index = 0; index < snake.length; index++) {
        context.strokeStyle="yellow";
        context.strokeRect(snake[index].x * snakeSize, snake[index].y * snakeSize, snakeSize, snakeSize);
        context.lineWidth= 7;
       if (snakeLength < 6) { 
        context.fillStyle = "orange";
        context.fillRect(snake[index].x * snakeSize, snake[index].y * snakeSize, snakeSize, snakeSize);
    }
  else if (snakeLength >= 6) {  
    context.fillStyle = "black";
        context.fillRect(snake[index].x * snakeSize, snake[1].y * snakeSize, snakeSize, snakeSize);
}
    }
}
function snakeUpdate() {
    var snakeHeadX = snake[0].x;
    var snakeHeadY = snake[0].y;
    if(snakeDirection == "down") {
    snakeHeadY++;
    }
    else if(snakeDirection == "right"){
       snakeHeadX++; 
    }
    else if(snakeDirection == "up") {
        snakeHeadY--;
    }
    else if(snakeDirection == "left") {
        snakeHeadX--;
    }    
        
        
        
        
    checkFoodCollisions(snakeHeadX, snakeHeadY);
    checkAppleCollisions(snakeHeadX, snakeHeadY);
    checkBlockCollisions(snakeHeadX, snakeHeadY);
    checkWallCollisions(snakeHeadX, snakeHeadY);
    checkSnakeCollisions(snakeHeadX, snakeHeadY);
    
    var snakeTail = snake.pop();
    snakeTail.x =  snakeHeadX;
    snakeTail.y = snakeHeadY;
    snake.unshift(snakeTail);
    
}




function snakeGrow() {
    for(var index = 0; index < snake.length; index++) {
    if(snakeLength < 6) {
      alert("hi");
  }     
  else if(snakeLength >= 7 < 8) {
      snakeSize = 7;
      context.fillRect(snake[index].x * snakeSize, snake[1].y * snakeSize, snakeSize, snakeSize);
     
  }
}
}
function snakeEnlarge() {
  
  if(snakeLength < 8) {
     alert("hey");
  }
  else if (snakeLength >= 8) {
      context.fillRect(snakeSize = 3);
  }
}
/*
 * -----------------------------------------------------------------------
 * Food Functions
 * -----------------------------------------------------------------------
 */

function foodInitialize() {
    food = {
       x: 0, 
       y: 0 
   };
     setFoodPosition();
}

function foodDraw() {
    context.fillStyle = "red";
    context.fillRect(food.x * snakeSize, food.y * snakeSize, snakeSize, snakeSize);
}

function setFoodPosition() {
    var randomX = Math.floor(Math.random() * screenWidth);
    var randomY = Math.floor(Math.random() * screenHeight);
    
    food.x = Math.floor(randomX / snakeSize);
    food.y = Math.floor(randomY / snakeSize);
}

function keyboardHandler(event) {
   console.log(event);
   
   if(event.keyCode == "39" && snakeDirection != "left") {
       snakeDirection = "right";
   }
   else if(event.keyCode == "40" && snakeDirection != "up") {
       snakeDirection = "down"; 
   }
   else if(event.keyCode == "37" && snakeDirection !=  "right") {
       snakeDirection = "left";
   }
   else if(event.keyCode == "38" && snakeDirection != "down") {
       snakeDirection = "up";
   }
}

function checkFoodCollisions(snakeHeadX, snakeHeadY) {
   if(snakeHeadX == food.x && snakeHeadY ==  food.y) {
       console.log("Food Collision");
       snake.push({
           x: 0,
           y: 0
       }); 
        snakeLength++;
       
        setFoodPosition(); 
         var randomX = Math.floor(Math.random() * screenWidth);
    var randomY = Math.floor(Math.random() * screenHeight);
    
    food.x = Math.floor(randomX / snakeSize);
    food.y = Math.floor(randomY / snakeSize);       
   }
}

function checkWallCollisions(snakeHeadX, snakeHeadY) {
    if(snakeHeadX * snakeSize >= screenWidth || snakeHeadX * snakeSize < 0) {
        console.log("Wall Collision");
        setState("GAME OVER");
    }
    else if(snakeHeadY * snakeSize >= screenHeight || snakeHeadY * snakeSize < 0){
       console.log("Wall Collision");
       setState("GAME OVER");
   } 
}

function checkSnakeCollisions(snakeHeadX, snakeHeadY) {
  for(var index = 1; index < snake.length; index++) {
   if(snakeHeadX == snake[index].x && snakeHeadY == snake[index].y) {
       setState("GAME OVER");
       return;
   }  
  }
}

function startState(menu) {
    startMenu = menu;
    showMenu(menu);
}

function setState(state) {
     gameState = state;
     showMenu(state);
     
}

function displayMenu(menu) {
    menu.style.visibility = "visible";
}

function hideMenu(menu) {
    menu.style.visibility = "hidden";     
}

function showMenu(state) {
    if(state == "GAME OVER") {
       displayMenu(gameOverMenu);
    }
    else if(state == "PLAY") {
       displayMenu(playHUD);
    }
    
}

function centerMenuPosition(menu) {
   menu.style.top = (screenHeight / 2) - (menu.offsetHeight / 2) + "px";
   menu.style.left = (screenWidth / 2) - (menu.offsetWidth / 2) + "px";
}

function drawScoreboard() {
    scoreboard.innerHTML = "Length: " + snakeLength;
}


function appleInitialize() {
    apple = {
       x: 0, 
       y: 0 
   };
     setApplePosition();
}

function appleDraw() {
    context.fillStyle = "yellow";
    context.fillRect(apple.x * snakeSize, apple.y * snakeSize, snakeSize, snakeSize);
}

function setApplePosition() {
    var appleX = Math.floor(Math.random() * screenWidth);
    var appleY = Math.floor(Math.random() * screenHeight);
    
    apple.x = Math.floor(appleX / snakeSize);
    apple.y = Math.floor(appleY / snakeSize);
}


function checkAppleCollisions(snakeHeadX, snakeHeadY) {
    for (var table = snakeLength - 1; table >= 0; table--) {
        snake.push({
            x: table,
            y: 0
        }); }
   if(snakeHeadX == apple.x && snakeHeadY ==  apple.y) {
       console.log("Apple Collision");
      
        snake.push({
            x: 0,
            y: 0
        });
       
       snakeLength++;
        
        setApplePosition(); 
         var appleX = Math.floor(Math.random() * screenWidth);
    var appleY = Math.floor(Math.random() * screenHeight);
    
    apple.x = Math.floor(appleX / snakeSize);
    apple.y = Math.floor(appleY / snakeSize);       
   }
}


function blockInitialize() {
    block = {
       x: 0, 
       y: 0 
   };
     setBlockPosition();
}

function blockDraw() {
    context.fillStyle = "red";
    context.fillRect(block.x * snakeSize, block.y * snakeSize, snakeSize, snakeSize);
}

function setBlockPosition() {
    var blockX = Math.floor(Math.random() * screenWidth);
    var blockY = Math.floor(Math.random() * screenHeight);
    
    block.x = Math.floor(blockX / snakeSize);
    block.y = Math.floor(blockY / snakeSize);
}


function checkBlockCollisions(snakeHeadX, snakeHeadY) {  
    for (var table = snakeLength - 1; table >= 0; table--) {
        snake.push({
            x: table,
            y: 0
        });
    }
   if(snakeHeadX == block.x && snakeHeadY == block.y) {                       
       console.log("Food Collision");
       snake.push({
           x: 0,
           y: 0
       }); 
        snakeLength--;
        
    


       
        setBlockPosition(); 
         var blockX = Math.floor(Math.random() * screenWidth);
    var blockY = Math.floor(Math.random() * screenHeight);
    
    block.x = Math.floor(blockX / snakeSize);
    block.y = Math.floor(blockY / snakeSize);       
    }
}



function snakeInitialize1() {
    snake1 = [];
    snakeLength1 = 5;
    snakeSize1 = 40;
    snakeDirection1 = "down";
    
    for(var index = snakeLength1 + 1; index >= 0; index--){
        snake1.push( {
            x: 20,
            y: 20
        } );
    }
}


function snakeDraw1() {
    for(var index = 0; index < snake1.length; index++) {
        
        context.strokeStyle="red";
        context.strokeRect(snake1[index].x * snakeSize1, snake1[index].y * snakeSize1, snakeSize1, snakeSize1);
        context.lineWidth= 7;
       if(snakeLength1 < 7) {
           context.fillStyle = "black";
           context.fillRect(snake1[index].x * snakeSize1, snake1[index].y * snakeSize1, snakeSize1, snakeSize1);
    }
    else if(snakeLength1 >= 7) {
        context.fillStyle = "red";
        context.fillRect(snake1[1].x * snakeSize1, snake1[index].y * snakeSize1, snakeSize1, snakeSize1);
    }
}
}

function snakeUpdate1() {
    var snakeHeadX1 = snake1[2].x;
    var snakeHeadY1 = snake1[0].y;
    if(snakeDirection1 == "down") {
    snakeHeadY1++;
    }
    else if(snakeDirection1 == "right"){
       snakeHeadX1++; 
    }
    else if(snakeDirection1 == "up") {
        snakeHeadY1--;
    }
    else if(snakeDirection1 == "left") {
        snakeHeadX1--;
    }    
        
        
        
        
    checkFoodCollisions(snakeHeadX1, snakeHeadY1);
    checkAppleCollisions(snakeHeadX1, snakeHeadY1);
    
    var snakeTail1 = snake1.pop();
    snakeTail1.x =  snakeHeadX1;
    snakeTail1.y = snakeHeadY1;
    snake1.unshift(snakeTail1);
    
}

  function keyboardHandler1(event) {
   console.log(event);
   
  

 if(event.keyCode == "37" && snakeDirection1 != "left") {
       snakeDirection1 = "right";
   }
   else if(event.keyCode == "38" && snakeDirection1 != "up") {
       snakeDirection1 = "down"; 
   }
   else if(event.keyCode == "39" && snakeDirection1 !=  "right") {
       snakeDirection1 = "left";
   }
   else if(event.keyCode == "40" && snakeDirection1 != "down") {
       snakeDirection1 = "up";
   }

  }





function snakeInitialize3() {
    snake3 = [];
    snakeLength3 = 5;
    snakeSize3 = 20;
    snakeDirection3 = "down";
    
    for(var index = snakeLength3 + 1; index >= 0; index--){
        snake3.push( {
            x: 45,
            y: 15
        } );
    }
}


function snakeDraw3() {
    for(var index = 0; index < snake3.length; index++) {
        
        context.strokeStyle="red";
        context.strokeRect(snake3[index].x * snakeSize3, snake3[index].y * snakeSize3, snakeSize3, snakeSize3);
        context.lineWidth= 7;
       if(snakeLength3 < 7) {
           context.fillStyle = "black";
           context.fillRect(snake3[index].x * snakeSize3, snake3[index].y * snakeSize3, snakeSize3, snakeSize3);
    }
    else if(snakeLength3 >= 7) {
        context.fillStyle = "red";
        context.fillRect(snake3[1].x * snakeSize3, snake3[index].y * snakeSize3, snakeSize3, snakeSize3);
    }
}
}

function snakeUpdate3() {
    var snakeHeadX3 = snake3[0].x;
    var snakeHeadY3 = snake3[6].y;
    if(snakeDirection3 == "down") {
    snakeHeadY3++;
    }
    else if(snakeDirection3 == "right"){
       snakeHeadX3++; 
    }
    else if(snakeDirection3 == "up") {
        snakeHeadY3--;
    }
    else if(snakeDirection3 == "left") {
        snakeHeadX3--;
    }    
        
        
        
        
    checkFoodCollisions(snakeHeadX3, snakeHeadY3);
    checkAppleCollisions(snakeHeadX3, snakeHeadY3);
    checkBlockCollisions(snakeHeadX3, snakeHeadY3);
    
    var snakeTail3 = snake3.pop();
    snakeTail3.x =  snakeHeadX3;
    snakeTail3.y = snakeHeadY3;
    snake3.unshift(snakeTail3);
    
}

  function keyboardHandler3(event) {
   console.log(event);
   
  

 if(event.keyCode == "37" && snakeDirection3 != "left") {
       snakeDirection3 = "right";
   }
   else if(event.keyCode == "38" && snakeDirection3 != "up") {
       snakeDirection3 = "down"; 
   }
   else if(event.keyCode == "39" && snakeDirection3 !=  "right") {
       snakeDirection3 = "left";
   }
   else if(event.keyCode == "40" && snakeDirection3 != "down") {
       snakeDirection3 = "up";
   }

  }
  
  

function snakeInitialize2() {
    snake2 = [];
    snakeLength2 = 5;
    snakeSize2 = 20;
    snakeDirection2 = "down";
    
    for(var index = snakeLength2 + 1; index >= 0; index--){
        snake2.push( {
            x: 40,
            y: 20
        } );
    }
}


function snakeDraw2() {
    for(var index = 0; index < snake2.length; index++) {
        
        context.strokeStyle="red";
        context.strokeRect(snake2[index].x * snakeSize2, snake2[index].y * snakeSize2, snakeSize2, snakeSize2);
        context.lineWidth= 7;
       if(snakeLength2 < 7) {
           context.fillStyle = "black";
           context.fillRect(snake2[index].x * snakeSize2, snake2[index].y * snakeSize2, snakeSize2, snakeSize2);
    }
    else if(snakeLength2 >= 7) {
        context.fillStyle = "red";
        context.fillRect(snake2[1].x * snakeSize2, snake2[index].y * snakeSize2, snakeSize2, snakeSize2);
    }
}
}

function snakeUpdate2() {
    var snakeHeadX2 = snake2[2].x;
    var snakeHeadY2 = snake2[4].y;
    if(snakeDirection2 == "down") {
    snakeHeadY2++;
    }
    else if(snakeDirection2 == "right"){
       snakeHeadX2++; 
    }
    else if(snakeDirection2 == "up") {
        snakeHeadY2--;
    }
    else if(snakeDirection2 == "left") {
        snakeHeadX2--;
    }    
        
        
        
        
    checkFoodCollisions(snakeHeadX2, snakeHeadY2);
    checkAppleCollisions(snakeHeadX2, snakeHeadY2);
    checkBlockCollisions(snakeHeadX2, snakeHeadY2);
    
    var snakeTail2 = snake2.pop();
    snakeTail2.x =  snakeHeadX2;
    snakeTail2.y = snakeHeadY2;
    snake2.unshift(snakeTail2);
    
}

  function keyboardHandler2(event) {
   console.log(event);
   
  

 if(event.keyCode == "39" && snakeDirection2 != "left") {
       snakeDirection2 = "right";
   }
   else if(event.keyCode == "38" && snakeDirection2 != "up") {
       snakeDirection2 = "down"; 
   }
   else if(event.keyCode == "37" && snakeDirection2 !=  "right") {
       snakeDirection2 = "left";
   }
   else if(event.keyCode == "40" && snakeDirection2 != "down") {
       snakeDirection2 = "up";
   }

  }
  
 

function snakeInitialize5() {
    snake5 = [];
    snakeLength5 = 5;
    snakeSize5 = 20;
    snakeDirection5 = "down";
    
    for(var index = snakeLength5 + 1; index >= 0; index--){
        snake5.push( {
            x: 9,
            y: 7
        } );
    }
}


function snakeDraw5() {
    for(var index = 0; index < snake5.length; index++) {
        
        context.strokeStyle="yellow";
        context.strokeRect(snake5[index].x * snakeSize5, snake5[index].y * snakeSize5, snakeSize5, snakeSize5);
        context.lineWidth= 7;
       if(snakeLength5 < 7) {
           context.fillStyle = "orange";
           context.fillRect(snake5[index].x * snakeSize5, snake5[index].y * snakeSize5, snakeSize5, snakeSize5);
    }
    else if(snakeLength5 >= 7) {
        context.fillStyle = "red";
        context.fillRect(snake5[1].x * snakeSize5, snake5[index].y * snakeSize5, snakeSize5, snakeSize5);
    }
}
}

function snakeUpdate5() {
    
    var snakeHeadX5 = snake5[6].x;
    var snakeHeadY5 = snake5[6].y;
    if(snakeDirection5 == "down") {
    snakeHeadY5++;
    }
    else if(snakeDirection5 == "right"){
       snakeHeadX5++; 
    }
    else if(snakeDirection5 == "up") {
        snakeHeadY5--;
    }
    else if(snakeDirection5 == "left") {
        snakeHeadX5--;
    }    
        
        
        
        
    checkFoodCollisions(snakeHeadX5, snakeHeadY5);
    checkAppleCollisions(snakeHeadX5, snakeHeadY5);
    
    var snakeTail5 = snake5.pop();
    snakeTail5.x =  snakeHeadX5;
    snakeTail5.y = snakeHeadY5;
    snake5.unshift(snakeTail5);
    
}

  function keyboardHandler5(event) {
   console.log(event);
   
  

 if(event.keyCode == "40" && snakeDirection5 != "left") {
       snakeDirection5 = "right";
   }
   else if(event.keyCode == "37" && snakeDirection5 != "up") {
       snakeDirection5 = "down"; 
   }
   else if(event.keyCode == "38" && snakeDirection5 !=  "right") {
       snakeDirection5 = "left";
   }
   else if(event.keyCode == "39" && snakeDirection5 != "down") {
       snakeDirection5 = "up";
   }

  }
 

