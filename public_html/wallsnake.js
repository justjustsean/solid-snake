/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */




var snake;
var snakeLength;
var snakeSize;
var snakeDirection;


var food;
var apple;
var block;
var border;

var context;
var screenWidth;
var screenHeight;


var gameState;
var gameOverMenu;
var restartButton;
var playHUD;
var scoreboard;
/*------------------------------------------------------------------------
 * Executing Game Code
 * -----------------------------------------------------------------------
 */

gameInitialize();
snakeInitialize();
foodInitialize();
appleInitialize();
blockInitialize();
borderInitialize();
setInterval(gameLoop, 1000/30);
/*
 * ----------------------------------------------------------------------
 *   Game Functions
 * -----------------------------------------------------------------------
 */

function gameInitialize() {
  var canvas = document.getElementById("game-screen");
  context = canvas.getContext("2d");
  
  screenWidth =  window.innerWidth;
  screenHeight = window.innerHeight;
  
  canvas.width = screenWidth;
  canvas.height = screenHeight;
  
  document.addEventListener("keydown", keyboardHandler);
  
  
  gameOverMenu = document.getElementById("gameOver");
  centerMenuPosition(gameOverMenu);
  
  restartButton = document.getElementById("restartButton");
  restartButton.addEventListener("click", gameRestart);
  
  playHUD = document.getElementById("playHUD");
  scoreboard =  document.getElementById("scoreboard");
  
  
  setState("PLAY"); 
  
}

function gameLoop() {
    gameDraw();
    drawScoreboard();
    if(gameState == "PLAY") {
    snakeUpdate();
    snakeDraw();
    foodDraw();
    appleDraw();
    blockDraw();
    borderDraw();
   }
   
}

function gameDraw() {
   context.fillStyle = "green"; 
   context.fillRect(0, 0, screenWidth, screenHeight);
}

function gameRestart() {
   snakeInitialize();
   foodInitialize();
   appleInitialize();
   blockInitialize();
   borderInitialize();
   hideMenu(gameOverMenu);
   setState("PLAY");
}

/*
 * ----------------------------------------------------------------------
 *   Snake Functions
 * -----------------------------------------------------------------------  
 */

function snakeInitialize() {
    snake = [];
    snakeLength = 5;
    snakeSize = 20;
    snakeDirection = "down";
    
    for(var index = snakeLength + 1; index >= 0; index--){
        snake.push( {
            x: index,
            y: 0
        } );
    }
}


function snakeDraw() {
    for(var index = 0; index < snake.length; index++) {
        context.fillStyle = "orange";
        context.fillRect(snake[index].x * snakeSize, snake[index].y * snakeSize, snakeSize, snakeSize);
    }
}

function snakeUpdate() {
    var snakeHeadX = snake[0].x;
    var snakeHeadY = snake[0].y;
    if(snakeDirection == "down") {
    snakeHeadY++;
    }
    else if(snakeDirection == "right"){
       snakeHeadX++; 
    }
    else if(snakeDirection == "up") {
        snakeHeadY--;
    }
    else if(snakeDirection == "left") {
        snakeHeadX--;
    }    
        
        
        
        
    checkFoodCollisions(snakeHeadX, snakeHeadY);
    checkAppleCollisions(snakeHeadX, snakeHeadY);
    checkBlockCollisions(snakeHeadX, snakeHeadY);
    checkBorderCollisions(snakeHeadX, snakeHeadY);
    checkWallCollisions(snakeHeadX, snakeHeadY);
    checkSnakeCollisions(snakeHeadX, snakeHeadY);
    
    var snakeTail = snake.pop();
    snakeTail.x =  snakeHeadX;
    snakeTail.y = snakeHeadY;
    snake.unshift(snakeTail);
    
}
/*
 * -----------------------------------------------------------------------
 * Food Functions
 * -----------------------------------------------------------------------
 */

function foodInitialize() {
    food = {
       x: 0, 
       y: 0 
   };
     setFoodPosition();
}

function foodDraw() {
    context.fillStyle = "red";
    context.fillRect(food.x * snakeSize, food.y * snakeSize, snakeSize, snakeSize);
}

function setFoodPosition() {
    var randomX = Math.floor(Math.random() * screenWidth);
    var randomY = Math.floor(Math.random() * screenHeight);
    
    food.x = Math.floor(randomX / snakeSize);
    food.y = Math.floor(randomY / snakeSize);
}

function keyboardHandler(event) {
   console.log(event);
   
   if(event.keyCode == "39" && snakeDirection != "left") {
       snakeDirection = "right";
   }
   else if(event.keyCode == "40" && snakeDirection != "up") {
       snakeDirection = "down"; 
   }
   else if(event.keyCode == "37" && snakeDirection !=  "right") {
       snakeDirection = "left";
   }
   else if(event.keyCode == "38" && snakeDirection != "down") {
       snakeDirection = "up";
   }
}

function checkFoodCollisions(snakeHeadX, snakeHeadY) {
   if(snakeHeadX == food.x && snakeHeadY ==  food.y) {
       console.log("Food Collision");
       snake.push({
           x: 0,
           y: 0
       }); 
        snakeLength++;
       
        setFoodPosition(); 
         var randomX = Math.floor(Math.random() * screenWidth);
    var randomY = Math.floor(Math.random() * screenHeight);
    
    food.x = Math.floor(randomX / snakeSize);
    food.y = Math.floor(randomY / snakeSize);       
   }
}

function checkWallCollisions(snakeHeadX, snakeHeadY) {
    if(snakeHeadX * snakeSize >= screenWidth || snakeHeadX * snakeSize < 0) {
        console.log("Wall Collision");
        setState("GAME OVER");
    }
    else if(snakeHeadY * snakeSize >= screenWidth || snakeHeadY * snakeSize < 0){
       console.log("Wall Collision");
       setState("GAME OVER");
   } 
}

function checkSnakeCollisions(snakeHeadX, snakeHeadY) {
  for(var index = 1; index < snake.length; index++) {
   if(snakeHeadX == snake[index].x && snakeHeadY == snake[index].y) {
       setState("GAME OVER");
       return;
   }  
  }
}

function startState(menu) {
    startMenu = menu;
    showMenu(menu);
}

function setState(state) {
     gameState = state;
     showMenu(state);
     
}

function displayMenu(menu) {
    menu.style.visibility = "visible";
}

function hideMenu(menu) {
    menu.style.visibility = "hidden";     
}

function showMenu(state) {
    if(state == "GAME OVER") {
       displayMenu(gameOverMenu);
    }
    else if(state == "PLAY") {
       displayMenu(playHUD);
    }
    
}

function centerMenuPosition(menu) {
   menu.style.top = (screenHeight / 2) - (menu.offsetHeight / 2) + "px";
   menu.style.left = (screenWidth / 2) - (menu.offsetWidth / 2) + "px";
}

function drawScoreboard() {
    scoreboard.innerHTML = "Length: " + snakeLength;
}


function appleInitialize() {
    apple = {
       x: 0, 
       y: 0 
   };
     setApplePosition();
}

function appleDraw() {
    context.fillStyle = "yellow";
    context.fillRect(apple.x * snakeSize, apple.y * snakeSize, snakeSize, snakeSize);
}

function setApplePosition() {
    var appleX = Math.floor(Math.random() * screenWidth);
    var appleY = Math.floor(Math.random() * screenHeight);
    
    apple.x = Math.floor(appleX / snakeSize);
    apple.y = Math.floor(appleY / snakeSize);
}


function checkAppleCollisions(snakeHeadX, snakeHeadY) {
   if(snakeHeadX == apple.x && snakeHeadY ==  apple.y) {
       console.log("Apple Collision");
       snake.push({
           x: 0,
           y: 0
       }); 
        snakeLength++;
        setApplePosition(); 
         var appleX = Math.floor(Math.random() * screenWidth);
    var appleY = Math.floor(Math.random() * screenHeight);
    
    apple.x = Math.floor(appleX / snakeSize);
    apple.y = Math.floor(appleY / snakeSize);       
   }
}


function blockInitialize() {
    block = {
       x: 0, 
       y: 0 
   };
     setBlockPosition();
}

function blockDraw() {
    context.fillStyle = "red";
    context.fillRect(block.x * snakeSize, block.y * snakeSize, snakeSize, snakeSize);
}

function setBlockPosition() {
    var blockX = Math.floor(Math.random() * screenWidth);
    var blockY = Math.floor(Math.random() * screenHeight);
    
    block.x = Math.floor(blockX / snakeSize);
    block.y = Math.floor(blockY / snakeSize);
}

function checkBlockCollisions(snakeHeadX, snakeHeadY) {
   if(snakeHeadX == block.x && snakeHeadY ==  block.y) {
       console.log("Block Collision");
        setState("GAME OVER");
    }
     
}


function borderInitialize() {
    border = {
       x: 0, 
       y: 0 
   };
     setBorderPosition();
}

function borderDraw() {
    context.fillStyle = "red";
    context.fillRect(border.x * snakeSize, border.y * snakeSize, snakeSize, snakeSize);
}

function setBorderPosition() {
    var borderX = Math.floor(Math.random() * screenWidth);
    var borderY = Math.floor(Math.random() * screenHeight);
    
    border.x = Math.floor(borderX / snakeSize);
    border.y = Math.floor(borderY / snakeSize);
}

function checkBorderCollisions(snakeHeadX, snakeHeadY) {
   if(snakeHeadX == border.x && snakeHeadY ==  border.y) {
       console.log("Border Collision");
        setState("GAME OVER");
    }
     
}

